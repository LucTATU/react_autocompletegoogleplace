# React Auto Completion Google Place 

### _Librairies:_
- npm install --save react-places-autocomplete

### _Source:_
- https://github.com/hibiken/react-places-autocomplete#utility-functions
- https://www.youtube.com/watch?v=pRiQeo17u6c

### _Tips:_
- Modify API KEY from public/index.html   

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBpgJJJkwHbzhvpIXP5M9uds6Y_yUOakU&libraries=places"></script>