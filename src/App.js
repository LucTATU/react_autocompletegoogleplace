import React, { useState } from "react";
import PlacesAutocomplete, { geocodeByAddress, geocodeByPlaceId, getLatLng } from "react-places-autocomplete";

function App() {
  const [address, setAddress] = useState("")
  const [coordinates, setCoordinates] = useState({
    lat: null,
    lng: null,
  })

  const handleSelect = async value => {
    const results = await geocodeByAddress(value)
    const li = await getLatLng(results[0])

    setAddress(value)
    setCoordinates(li)
  }

  return (
    <div className="App">
      <p>Latitude: {coordinates.lat} </p>
      <p>Longitude: {coordinates.lng} </p>
      <p>Address: {address} </p>
    
    <PlacesAutocomplete
      value={address}
      onChange={setAddress}
      onSelect={handleSelect}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div   
        key={suggestions.description}
        >
          <input
            {...getInputProps({
              placeholder: "Search Places ...",
              className: "location-search-input",
            })}
          />
          <div className="autocomplete-dropdown-container">
            {loading && <div>Loading...</div>}
            {suggestions.map((suggestion) => {
              const className = suggestion.active
                ? "suggestion-item--active"
                : "suggestion-item";
              // inline style for demonstration purpose
              const style = suggestion.active
                ? { backgroundColor: "#fafafa", cursor: "pointer" }
                : { backgroundColor: "#ffffff", cursor: "pointer" };
              return (
                <div
                  {...getSuggestionItemProps(suggestion, {
                    className,
                    style,
                  })}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </PlacesAutocomplete>

    </div>
  );
}

export default App;
